<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/')->name('home');
Route::get('/checkMember','UrlShortenerController@checkMember');
Route::get('/checkTransaction','UrlShortenerController@checkTransaction');

Route::get('/{key}', 'UrlShortenerController@index');