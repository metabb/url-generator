<?php

namespace App\Http\Controllers;

use App\Eloquent\Repository\LogRepository;
use App\Eloquent\Repository\UrlRepository;
use App\Helper\CurrentEmployeeTrait;
use App\Service\UrlShortenerService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class UrlShortenerController
 *
 * @package App\Http\Controllers
 */
class UrlShortenerController extends Controller
{
    use CurrentEmployeeTrait;

    /**
     * @var UrlRepository
     */
    private $urlRepository;

    /**
     * @var LogRepository
     */
    private $logRepository;

    /**
     * @var UrlShortenerService
     */
    private $urlShortenerService;

    /**
     * UrlShortenerController constructor.
     *
     * @param UrlRepository       $urlRepository
     * @param LogRepository       $logRepository
     * @param UrlShortenerService $urlShortenerService
     */
    public function __construct(
        UrlRepository $urlRepository,
        LogRepository $logRepository,
        UrlShortenerService $urlShortenerService
    ) {
        $this->urlRepository       = $urlRepository;
        $this->logRepository       = $logRepository;
        $this->urlShortenerService = $urlShortenerService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function index(Request $request,$key)
    {
        
        if (empty($key)) {
            return view('app/url/visit');
        }
        $url = $this->urlRepository->fetchByKey($key)->first();
        if (empty($url)) {
            return view('app/url/visit');
        }

        $this->urlShortenerService->visitUrl($url, $request);

        return redirect($url->original_url);
    }

    /**
     * @return Factory|View
     */
    public function list()
    {
        $urls = $this->urlRepository->fetchAll()->byUser($this->getCurrentUser()->id)->paginate(10);

        return view('app/url/list', ['urls' => $urls]);
    }

    /**
     * @param mixed $urlId
     *
     * @return Factory|View
     */
    public function show($urlId)
    {
        $url = $this->urlRepository->fetchById((int)$urlId)->byUser($this->getCurrentUser()->id)->first();
        if (empty($url)) {
            abort(404, 'Url is not found');
        }

        $logs = $this->logRepository->fetchByUrl($url->id)->paginate(10);

        return view('app/url/show', ['url' => $url, 'logs' => $logs]);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('app/url/create');
    }

    // public function checkMember()
    
    // {

    //     $reff_id = 'asd';
    //     $bankadmins = DB::table('cms_users')->where('reff_code', $reff_id)->first();
    //     $account_member = DB::table('account_member')->where('ref_code', $reff_id)->first();
    //     if($bankadmins == ""){
    //         return response()->json([
    //             'code' => '0',
    //             'info' => 'Data Not Found !'
    //         ]);
    //     }else if($bankadmins != "" && $account_member == ""){
    //         return response()->json([
    //             'code' => '1',
    //             'info' => 'Success !'
    //         ]);
    //     }else if($bankadmins != "" && $account_member != ""){
    //         return response()->json([
    //             'code' => '2',
    //             'info' => 'Data Duplicated !'
    //         ]);
    //     }
    // }

    public function checkMember()
    {
        $response = Curl::to('http://35.236.180.98:3000/banks/statement?bankName=')->get();
        $decode = json_decode($response, true);
        $member = $decode['member'];
        
        foreach ($member as $mem) {
            $bankadmins = DB::table('cms_users')->where('reff_code', $mem['ref_code'])->first();
            $account_member = DB::table('account_member')->where('ref_code', $mem['ref_code'])->first();
            if($bankadmins == ""){
                return response()->json([
                    'code' => '0',
                    'info' => 'Data Not Found !'
                ]);
            }else if($bankadmins != "" && $account_member == ""){
                DB::table('users')->insert(
                    ['account_code' => $mem['account_code'], 'account_name' => $mem['account_name'],'ip_address'=>$mem['ip_address'],'ref_code'=> $mem['ref_code']]
                );

                return response()->json([
                    'code' => '1',
                    'info' => 'Success !'
                ]);
            }else if($bankadmins != "" && $account_member != ""){
                return response()->json([
                    'code' => '2',
                    'info' => 'Data Duplicated !'
                ]);
            }
        }
    }


    public function checkTransaction()
    {
        $response = Curl::to('http://35.236.180.98:3000/banks/statement?bankName=')->get();
        $decode = json_decode($response, true);
        $transaction = $decode['transaction'];
        
        foreach ($transaction as $trans) {
            $transaction_history = DB::table('transaction_history')->where('transaction_id', $trans['transaction_id'])->first();
            $account_member = DB::table('account_member')->where('ref_code', $trans['ref_no'])->first();
            if($account_member == ""){
                return response()->json([
                    'code' => '0',
                    'info' => 'Data Not Found !'
                ]);
            }else if($transaction_history == "" && $account_member != ""){
                DB::table('transaction_history')->insert(
                    ['transaction_id' => $trans['transaction_id'], 'account_name' => $trans['account_name'],'account_code'=>$trans['account_code'],'bank_name'=>$trans['bank_name'],'ref_no'=> $trans['ref_no'],'ammount'=>$trans['ammount'],'confirmed_by'=>$trans['confirmed_by'],'confirmation_time'=>$trans['confirmation_time']]
                );

                return response()->json([
                    'code' => '1',
                    'info' => 'Success !'
                ]);
            }else if($transaction_history != "" && $account_member != ""){
                return response()->json([
                    'code' => '2',
                    'info' => 'Data Duplicated !'
                ]);
            }
        }
    }

}
