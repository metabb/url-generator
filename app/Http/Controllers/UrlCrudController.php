<?php

namespace App\Http\Controllers;

use App\Eloquent\Repository\UrlRepository;
use App\Helper\CurrentEmployeeTrait;
use App\Http\Controllers\Request\UrlRequest;
use App\Service\UrlService;
use App\Service\UrlShortenerService;
use Illuminate\Http\RedirectResponse;

/**
 * Class UrlCrudController
 *
 * @package App\Http\Controllers
 */
class UrlCrudController extends Controller
{
    use CurrentEmployeeTrait;

    /**
     * @var UrlService
     */
    private $urlService;

    /**
     * @var UrlShortenerService
     */
    private $urlShortenerService;

    /**
     * @var UrlRepository
     */
    private $urlRepository;

    /**
     * UrlCrudController constructor.
     *
     * @param UrlService          $urlService
     * @param UrlShortenerService $urlShortenerService
     * @param UrlRepository       $urlRepository
     */
    public function __construct(
        UrlService $urlService,
        UrlShortenerService $urlShortenerService,
        UrlRepository $urlRepository
    ) {
        $this->urlService          = $urlService;
        $this->urlShortenerService = $urlShortenerService;
        $this->urlRepository       = $urlRepository;
    }

    /**
     * @param UrlRequest $urlRequest
     *
     * @return RedirectResponse
     */
    public function create(UrlRequest $urlRequest)
    {
        try {
            $attributes        = $urlRequest->all();
            $attributes['key'] = $this->urlShortenerService->generateKey();
            $url               = $this->urlService->create($attributes);

            return redirect()->route('show.url', ['urlId' => $url->id]);
        } catch (\Exception $exception) {
            abort(500, 'Error occurred during saving url');
        }
    }

    /**
     *
     *
     * @return RedirectResponse
     */
    public function delete($urlId)
    {
        $url = $this->urlRepository->fetchById((int)$urlId)->byUser($this->getCurrentUser()->id)->first();
        if (empty($url)) {
            abort(404, 'Url not found');
        }
        $this->urlService->delete($url);

        return redirect()->route('urls');
    }
}
