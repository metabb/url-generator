<?php

namespace App\Service;

use App\Eloquent\Repository\UrlRepository;
use App\Entity\Url;
use App\Helper\CurrentEmployeeTrait;

class UrlService
{
    use CurrentEmployeeTrait;

    /**
     * @var UrlRepository
     */
    private $urlRepository;

    /**
     * UrlShortenerService constructor.
     *
     * @param UrlRepository $urlRepository
     */
    public function __construct(UrlRepository $urlRepository)
    {
        $this->urlRepository = $urlRepository;
    }

    /**
     * @param array $attributes
     *
     * @return Url
     */
    public function create(array $attributes): Url
    {
        $url = (new Url())->fill($attributes);
        $url->user()->associate($this->getCurrentUser());
        $url->save();

        return $url;
    }

    /**
     * @param Url $url
     */
    public function delete(Url $url)
    {
        $url->delete();
    }
}
