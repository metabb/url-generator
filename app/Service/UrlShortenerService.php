<?php

namespace App\Service;

use App\Eloquent\Repository\UrlRepository;
use App\Entity\Url;
use Illuminate\Http\Request;

class UrlShortenerService
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var UrlRepository
     */
    private $urlRepository;

    /**
     * @var LogService
     */
    private $logService;

    /**
     * UrlShortenerService constructor.
     *
     * @param UrlRepository $urlRepository
     * @param LogService    $logService
     *
     * @internal param UrlService $urlService
     */
    public function __construct(UrlRepository $urlRepository, LogService $logService)
    {
        $this->urlRepository = $urlRepository;
        $this->logService    = $logService;
        $this->config        = config('urlshortener');
    }

    /**
     * @param Url     $url
     * @param Request $request
     */
    public function visitUrl(Url $url, Request $request)
    {
        $url->increment('visit_count');
        $this->logService->create($url, $request);
    }

    /**
     * @return string
     */
    public function generateKey(): string
    {
        do {
            $key = "";

            $characters = preg_split('//', $this->config['characters']);
            $length     = $this->config['key_length'];

            for ($i = 0; $i < $length; $i++) {
                $key .= $characters[random_int(0, count($characters) - 1)];
            }

            $checkCode = $this->checkKey($key);
        } while (!$checkCode);

        return $key;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    private function checkKey(string $key): bool
    {
        $url = $this->urlRepository->fetchByKey($key)->first();

        return empty($url);
    }
}
